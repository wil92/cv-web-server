/**
 * Created by oreste on 26/09/17.
 */
var _ = require('lodash');

module.exports = {
  atributes:{
    key:{
      type: 'string'
    },
    value:{
      type: 'string'
    },
    owner:{
      model: 'section'
    }
  }
};
