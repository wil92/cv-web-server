"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by oreste on 20/09/17.
 */
var core_1 = require("@angular/core");
var section_factory_1 = require("../class/factory/section.factory");
var DataService = (function () {
    function DataService() {
        //********************************************************
        //*                 ACTIONS WITH SECTIONS                    *
        //********************************************************
        this._sectionList = [];
        this._sectionListLink = [];
    }
    Object.defineProperty(DataService.prototype, "sectionList", {
        get: function () {
            return this._sectionList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DataService.prototype, "sectionListLink", {
        get: function () {
            return this._sectionListLink;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get a post by the id.
     * @param identify Identify of the section to return;
     * @returns {SectionBase<T>} SectionBase finded if exist or 'undefined' in other case.
     */
    DataService.prototype.getSectionById = function (key) {
        console.log('pidiendo section por id');
        var position = this.sectionListLink[key];
        if (position != undefined) {
            return this.sectionList[position];
        }
        return undefined;
    };
    /**
     * For insert post to the list for a given section.
     * @param tab Tab to insert.
     */
    DataService.prototype.addSection = function (section) {
        console.log('el nombre de la section agregada es ' + section.nameSection);
        if (this.sectionListLink[section.id]) {
            var position = this.sectionListLink[section.id];
            this.sectionList[position] = section;
            return;
        }
        this.sectionListLink[section.id] = this.sectionList.length;
        this.sectionList.push(section);
    };
    /**
     * For insert a list of section to the list.
     * @param tabs List of sections.
     */
    DataService.prototype.addAllSections = function (sections) {
        console.log('entra a dataservice');
        for (var i = 0; i < sections.length; i++) {
            try {
                var base = sections[i];
                this.addSection(section_factory_1.buildSection(base));
            }
            catch (err) {
                console.log(err.message);
            }
        }
    };
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=datas.service.js.map