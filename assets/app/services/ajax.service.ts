/**
 * Created by guille on 31/08/17.
 */
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";


@Injectable()
export class AjaxService{
  constructor(public http: Http){ }

  /**
   * Handle the singin responce.
   * @param data User datas for singin.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  sendSection(data : any){
    console.log('ooooooooooooooooooooooooooooooooo'+data);
    return this.http.post('section/crea', data, {});
    // this.http.post('section/crea',data, {}).subscribe((response)=>{
    //   console.log('la respuesta de enviar el section es '+ response.json()['id']);
    // });
  }

  /**
   * Handle the user list responce.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  listUser(){
    return this.http.get('user');
  }

  /**
   * Handle the user data responce.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  userData(){
    return this.http.get('user/datas');
  }

  /**
   * Handle post create template.
   * @param createTemplate Identify of the post create template.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  postCreateTempate(createTemplate: string){
    return this.http.get('templates/posts/create/' + createTemplate);
  }

  /**
   * Handle post show template.
   * @param showTemplate Identify of the post show template.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  postShowTempate(showTemplate: string){
    return this.http.get('templates/posts/show/' + showTemplate);
  }

  /**
   * Handle the post list responce.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  postList(tabOwner: string){
    return this.http.get('post/list2?tabOwner=' + tabOwner);
  }

  /**
   * Handle the tab list responce.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  tabList(){
    return this.http.get('tab/list2');
  }

  getSection(){
    return this.http.get('section/list');
  }

  /**
   * Handle tab show template.
   * @param showTemplate Identify of the tab show template.
   * @returns {Observable<Response>} Observable with the responce for handle.
   */
  tabShowTemplate(showTemplate: string){
    return this.http.get('templates/tabs/' + showTemplate);
  }
}
