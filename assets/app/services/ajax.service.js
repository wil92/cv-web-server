"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by guille on 31/08/17.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var AjaxService = (function () {
    function AjaxService(http) {
        this.http = http;
    }
    /**
     * Handle the singin responce.
     * @param data User datas for singin.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.sendSection = function (data) {
        console.log('ooooooooooooooooooooooooooooooooo' + data);
        return this.http.post('section/crea', data, {});
        // this.http.post('section/crea',data, {}).subscribe((response)=>{
        //   console.log('la respuesta de enviar el section es '+ response.json()['id']);
        // });
    };
    /**
     * Handle the user list responce.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.listUser = function () {
        return this.http.get('user');
    };
    /**
     * Handle the user data responce.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.userData = function () {
        return this.http.get('user/datas');
    };
    /**
     * Handle post create template.
     * @param createTemplate Identify of the post create template.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.postCreateTempate = function (createTemplate) {
        return this.http.get('templates/posts/create/' + createTemplate);
    };
    /**
     * Handle post show template.
     * @param showTemplate Identify of the post show template.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.postShowTempate = function (showTemplate) {
        return this.http.get('templates/posts/show/' + showTemplate);
    };
    /**
     * Handle the post list responce.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.postList = function (tabOwner) {
        return this.http.get('post/list2?tabOwner=' + tabOwner);
    };
    /**
     * Handle the tab list responce.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.tabList = function () {
        return this.http.get('tab/list2');
    };
    AjaxService.prototype.getSection = function () {
        return this.http.get('section/list');
    };
    /**
     * Handle tab show template.
     * @param showTemplate Identify of the tab show template.
     * @returns {Observable<Response>} Observable with the responce for handle.
     */
    AjaxService.prototype.tabShowTemplate = function (showTemplate) {
        return this.http.get('templates/tabs/' + showTemplate);
    };
    AjaxService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AjaxService);
    return AjaxService;
}());
exports.AjaxService = AjaxService;
//# sourceMappingURL=ajax.service.js.map