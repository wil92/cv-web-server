/**
 * Created by oreste on 20/09/17.
 */
import {Injectable} from "@angular/core";
import {SectionBase} from "../class/section-base";
import {buildSection} from "../class/factory/section.factory";

@Injectable()
export class DataService{

  constructor(){ }

  //********************************************************
  //*                 ACTIONS WITH SECTIONS                    *
  //********************************************************

  private _sectionList: SectionBase[] = [];
  private _sectionListLink: number[] = [];

  get sectionList(): SectionBase[] {
    return this._sectionList;
  }

  get sectionListLink(): number[] {
    return this._sectionListLink;
  }

  /**
   * Get a post by the id.
   * @param identify Identify of the section to return;
   * @returns {SectionBase<T>} SectionBase finded if exist or 'undefined' in other case.
   */
  getSectionById(key: string): SectionBase{
    console.log('pidiendo section por id');
    let position: number = this.sectionListLink[ key ];
    if(position != undefined){
      return this.sectionList[ position ];
    }

    return undefined;
  }
  /**
   * For insert post to the list for a given section.
   * @param tab Tab to insert.
   */
  addSection(section: SectionBase): void{
    console.log('el nombre de la section agregada es '+section.nameSection);
    if(this.sectionListLink[ section.id ]){
      let position : number = this.sectionListLink[ section.id ];
      this.sectionList[ position ] = section;
      return;
    }
    this.sectionListLink[section.id] = this.sectionList.length;
    this.sectionList.push(section);
  }

  /**
   * For insert a list of section to the list.
   * @param tabs List of sections.
   */
  addAllSections(sections: any[]): void{
    console.log('entra a dataservice');
    for(let i=0;i<sections.length;i++){
      try{
        let base = sections[i];
        this.addSection(buildSection(base));
      }catch (err){console.log(err.message);}
    }
  }

  //********************************************************
}
