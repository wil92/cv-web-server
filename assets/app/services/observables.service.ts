import {Injectable} from "@angular/core";
import {Observable, Observer, Subject} from "rxjs";
/**
 * Created by oreste on 20/09/17.
 */
@Injectable()
export class ObservablesService {
  constructor() {
    this.initObservers();
  }

  // Create Observer instances ----- BEGIN
  public linkSectionObservable: Observable<any>;
  private linkSectionObserver: Observer<any>;

  linkSectionStateChangeEmit(state: {key: string}): void{
    this.linkSectionObserver.next(state);
  }

  initObservers(): void{
    this.linkSectionObserver=new Subject();
    this.linkSectionObservable = new Observable((observer: any) => this.linkSectionObserver = observer);
  }
}
