"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var core_1 = require('@angular/core');
var app_component_1 = require("../components/app.component");
var section_skill_component_1 = require("../components/section.skill.component");
var http_1 = require("@angular/http");
var observables_service_1 = require("../services/observables.service");
var datas_service_1 = require("../services/datas.service");
var ajax_service_1 = require("../services/ajax.service");
var section_profile_component_1 = require("../components/section.profile.component");
var section_show_component_1 = require("../components/section.show.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.ReactiveFormsModule, http_1.HttpModule, forms_1.FormsModule],
            declarations: [app_component_1.AppComponent, section_skill_component_1.CompSkill, section_profile_component_1.CompProfile, section_show_component_1.SectionItem],
            bootstrap: [app_component_1.AppComponent],
            providers: [observables_service_1.ObservablesService, datas_service_1.DataService, ajax_service_1.AjaxService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map