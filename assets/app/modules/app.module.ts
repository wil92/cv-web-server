import { BrowserModule }                from '@angular/platform-browser';
import {ReactiveFormsModule, FormsModule}          from '@angular/forms';
import { NgModule }                     from '@angular/core';
import {AppComponent} from "../components/app.component";
import {CompSkill} from "../components/section.skill.component";
import {HttpModule} from "@angular/http";
import {ObservablesService} from "../services/observables.service";
import {DataService} from "../services/datas.service";
import {AjaxService} from "../services/ajax.service";
import {CompProfile} from "../components/section.profile.component";
import {SectionItem} from "../components/section.show.component";


@NgModule({
  imports: [ BrowserModule, ReactiveFormsModule, HttpModule, FormsModule ],
  declarations: [ AppComponent, CompSkill, CompProfile, SectionItem ],
  bootstrap: [ AppComponent ],
  providers: [ObservablesService, DataService, AjaxService]
})
export class AppModule {
  constructor() {
  }
}
