import {SectionBase} from "../class/section-base";
import {Component, Input} from '@angular/core'
/**
 * Created by oreste on 26/09/17.
 */
@Component({
  selector: 'section-item',
  templateUrl: 'templates/section_show.html'
})

export class SectionItem{
  @Input() section: SectionBase;
}
