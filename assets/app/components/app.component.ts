import { Component }       from '@angular/core';

import {AjaxService} from "../services/ajax.service";
import {DataService} from "../services/datas.service";
import {SectionBase} from "../class/section-base";
import {ObservablesService} from "../services/observables.service";
import {buildSectionSkill} from "../class/factory/section.skill.factory";
import {buildSection} from "../class/factory/section.factory";

@Component({
  selector: 'my-ap',
  templateUrl: 'templates/group-sections.html',
  providers:  [AjaxService, DataService, ObservablesService]
})
export class AppComponent {
  section: SectionBase;
  sections: SectionBase[];
  sectionsTittle: any = [
    {value: 'Add Skill', key: 'skill'},
    {value: 'Add profile', key: 'profile'}
  ];
  selectedValue: string;

  constructor(private ajax: AjaxService, private data: DataService, private observers: ObservablesService) {
    this.ajax.getSection().subscribe(data => {
      let list = data.json();
      this.data.addAllSections(list);
    });
    this.sections = this.data.sectionList;
  }
  public addi(){
    let newSection = this.data.getSectionById(this.selectedValue);
    if(newSection == undefined)
      this.section=buildSection({key: this.selectedValue});else
    this.section = buildSection(newSection);
    console.log("mostrando la section "+this.section);
    console.log('presionando boton save');
    this.observers.linkSectionStateChangeEmit({key: this.selectedValue});
  }

}
