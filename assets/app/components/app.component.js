"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ajax_service_1 = require("../services/ajax.service");
var datas_service_1 = require("../services/datas.service");
var observables_service_1 = require("../services/observables.service");
var section_factory_1 = require("../class/factory/section.factory");
var AppComponent = (function () {
    function AppComponent(ajax, data, observers) {
        var _this = this;
        this.ajax = ajax;
        this.data = data;
        this.observers = observers;
        this.sectionsTittle = [
            { value: 'Add Skill', key: 'skill' },
            { value: 'Add profile', key: 'profile' }
        ];
        this.ajax.getSection().subscribe(function (data) {
            var list = data.json();
            _this.data.addAllSections(list);
        });
        this.sections = this.data.sectionList;
    }
    AppComponent.prototype.addi = function () {
        var newSection = this.data.getSectionById(this.selectedValue);
        if (newSection == undefined)
            this.section = section_factory_1.buildSection({ key: this.selectedValue });
        else
            this.section = section_factory_1.buildSection(newSection);
        console.log("mostrando la section " + this.section);
        console.log('presionando boton save');
        this.observers.linkSectionStateChangeEmit({ key: this.selectedValue });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-ap',
            templateUrl: 'templates/group-sections.html',
            providers: [ajax_service_1.AjaxService, datas_service_1.DataService, observables_service_1.ObservablesService]
        }), 
        __metadata('design:paramtypes', [ajax_service_1.AjaxService, datas_service_1.DataService, observables_service_1.ObservablesService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map