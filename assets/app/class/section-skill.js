"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Created by oreste on 20/09/17.
 */
var section_base_1 = require("./section-base");
var SectionSkill = (function (_super) {
    __extends(SectionSkill, _super);
    function SectionSkill(key, nameSkill, descriptionSkill) {
        _super.call(this, key, 'Skills', nameSkill, descriptionSkill);
        this.strength = 10;
    }
    SectionSkill.prototype.getJson = function () {
        return {
            key: 'skill',
            nameSkill: this.nameSkill,
            descriptionSkill: this.descriptionSkill,
            strength: this.strength };
    };
    SectionSkill.prototype.setParametersSuper = function () {
        _super.prototype.setParameters.call(this, this.nameSkill, this.descriptionSkill);
    };
    return SectionSkill;
}(section_base_1.SectionBase));
exports.SectionSkill = SectionSkill;
//# sourceMappingURL=section-skill.js.map