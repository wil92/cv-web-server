/**
 * Created by oreste on 20/09/17.
 */
export class SectionBase{
  key: string;
  tittle: string;
  nameSection: string;
  descriptionSection: string;
  id: number;
  jobTitle: string;
  jobCompany: string;
  jobDescription: string;
  jobStartDate: string;
  jobEndDate: string;
  controlType: string;
  label: string;
  identify: string;


  constructor (key: string, tittle: string, nameSection?: string, descriptionSection?: string){
    // this.value=options.value;
    // this.key=options.key;
    // this.fullName=options.fullName || '';
    // this.email=options.email || '';
    // this.phone=options.phone || '';
    // this.strength=options.strength === undefined ? 1 : options.strength;
    // this.jobTitle=options.jobTitle || '';
    // this.jobCompany=options.jobCompany || '';
    // this.jobDescription=options.jobDescription || '';
    // this.jobStartDate=options.jobStartDate || '';
    // this.jobEndDate=options.jobEndDate || '';
    // this.controlType=options.controlType;
    // this.label=options.label;
    // this.identify=options.identify;
    this.key=key;
    this.tittle=tittle;
    this.nameSection=nameSection;
    this.descriptionSection=descriptionSection;
  }

  protected setParameters(nameSection: string, descriptionSection: string){
    this.nameSection = nameSection;
    this.descriptionSection = descriptionSection;
  }

}
