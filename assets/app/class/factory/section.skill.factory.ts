import {SectionSkill} from "../section-skill";
/**
 * Created by oreste on 20/09/17.
 */
export function  buildSectionSkill(datas: any): SectionSkill{
  let newSectionSkill = (datas.nameSkill)
                            ?new SectionSkill(datas.key, datas.nameSkill, datas.descriptionSkill)
                            :new SectionSkill(datas.key);

  newSectionSkill.id = datas.id;
  newSectionSkill.nameSkill=datas.nameSkill || "";
  newSectionSkill.descriptionSkill=datas.descriptionSkill || "";
  newSectionSkill.strength=datas.strength || 1;

  return newSectionSkill;
}
