"use strict";
var section_skill_1 = require("../section-skill");
/**
 * Created by oreste on 20/09/17.
 */
function buildSectionSkill(datas) {
    var newSectionSkill = (datas.nameSkill)
        ? new section_skill_1.SectionSkill(datas.key, datas.nameSkill, datas.descriptionSkill)
        : new section_skill_1.SectionSkill(datas.key);
    newSectionSkill.id = datas.id;
    newSectionSkill.nameSkill = datas.nameSkill || "";
    newSectionSkill.descriptionSkill = datas.descriptionSkill || "";
    newSectionSkill.strength = datas.strength || 1;
    return newSectionSkill;
}
exports.buildSectionSkill = buildSectionSkill;
//# sourceMappingURL=section.skill.factory.js.map