import {SectionProfile} from "../section-profile";
/**
 * Created by oreste on 22/09/17.
 */
export function buildSectionProfile(datas?: any){
  let newSectionProfile = new SectionProfile(datas.key);

  newSectionProfile.fullName=datas.fullName || "";
  newSectionProfile.email=datas.email || "";
  newSectionProfile.phone=datas.phone || "";

  return newSectionProfile;
}
