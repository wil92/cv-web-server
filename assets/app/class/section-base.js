"use strict";
/**
 * Created by oreste on 20/09/17.
 */
var SectionBase = (function () {
    function SectionBase(key, tittle, nameSection, descriptionSection) {
        // this.value=options.value;
        // this.key=options.key;
        // this.fullName=options.fullName || '';
        // this.email=options.email || '';
        // this.phone=options.phone || '';
        // this.strength=options.strength === undefined ? 1 : options.strength;
        // this.jobTitle=options.jobTitle || '';
        // this.jobCompany=options.jobCompany || '';
        // this.jobDescription=options.jobDescription || '';
        // this.jobStartDate=options.jobStartDate || '';
        // this.jobEndDate=options.jobEndDate || '';
        // this.controlType=options.controlType;
        // this.label=options.label;
        // this.identify=options.identify;
        this.key = key;
        this.tittle = tittle;
        this.nameSection = nameSection;
        this.descriptionSection = descriptionSection;
    }
    SectionBase.prototype.setParameters = function (nameSection, descriptionSection) {
        this.nameSection = nameSection;
        this.descriptionSection = descriptionSection;
    };
    return SectionBase;
}());
exports.SectionBase = SectionBase;
//# sourceMappingURL=section-base.js.map