/**
 * Created by oreste on 20/09/17.
 */
import {SectionBase} from "./section-base";
export class SectionSkill extends SectionBase {
  nameSkill: string;
  descriptionSkill: string;
  strength: number = 10;

  getJson(): any {
    return {
      key:'skill',
      nameSkill:this.nameSkill,
      descriptionSkill:this.descriptionSkill,
      strength:this.strength};
  }

  public setParametersSuper(){
    super.setParameters(this.nameSkill, this.descriptionSkill);
  }

  constructor(key: string, nameSkill?: string, descriptionSkill?: string) {
    super(key,'Skills',nameSkill,descriptionSkill);
  }
}
