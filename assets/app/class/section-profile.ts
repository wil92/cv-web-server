import {SectionBase} from "./section-base";
/**
 * Created by oreste on 22/09/17.
 */
export class SectionProfile extends SectionBase{
  fullName: string;
  email: string;
  phone: string;

  constructor(key: string){
    super(key);
  }
}
